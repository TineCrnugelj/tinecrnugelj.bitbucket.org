var bol = [];

var circle = null;
var line = null;

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1);
  var a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

function rad(x) {return x*Math.PI/180;}

function initMap() {

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {
            lat: 46.059115,
            lng: 14.509942
        }
    });

    $.getJSON("https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", function(data) {
        data.features.forEach(function(feat) {
            if (feat.properties["addr:city"] == "Ljubljana"){
                var cords = feat.geometry.coordinates[0];
                if (cords[0] != null) {
                    var marker = new google.maps.Marker({
                        position: {
                            lat: cords[0][1],
                            lng: cords[0][0]
                        },
                        map: map,
                        title: feat.properties.name
                    });
                    var infowindow = new google.maps.InfoWindow({
                      content: `<h4>${feat.properties.name}</h4><p>${feat.properties['addr:street']} ${feat.properties['addr:housenumber']}</p>`
                    });
                    marker.addListener('click', function(){
                        var pos = marker.getPosition();
                        map.setCenter(marker.getPosition());
                        infowindow.open(map, marker);
                    });

                    var poligon = [];
                    cords.forEach(function(cord) {
                        poligon.push({
                            lat: cord[1],
                            lng: cord[0]
                        });
                    });

                    var area = new google.maps.Polygon({
                        paths: poligon,
                        strokeWeight: 1,
                        fillColor: 'blue'
                    });
                    area.setMap(map);


                    bol.push({'marker': marker, 'area': area});
                }
            }
        });
        google.maps.event.addListener(map, 'click', function ( event ) {
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();

            if(line == null){
                line = new google.maps.Polyline({
                   strokeColor: '#000000',
                   strokeOpacity: 1.0,
                   strokeWeight: 3,
                   map: map
                 });
            }

            if(circle == null)
                 circle = new google.maps.Circle({
                     fillColor: 'green',
                     map: map,
                    center: {
                        lat: lat,
                        lng: lng
                    },
                     radius:1000
                   });
            else {
                circle.setCenter({ lat: lat, lng: lng })
            }

            var distance = 0;

            bol.forEach(function(b){
                var mlat = b.marker.position.lat();
                var mlng = b.marker.position.lng();

                var dis = getDistanceFromLatLonInKm(
                    lat, lng,
                    mlat, mlng,
                );

                if(distance == 0) distance = dis;
                else if(distance > dis) {
                    distance = dis;
                    line.setPath([
                        {lat: mlat, lng: mlng},
                        {lat: lat, lng: lng},
                    ]);
                }

                if(dis < 1){
                    b.area.setOptions({strokeWeight: 2.0, fillColor: 'green'});
                } else {
                    b.area.setOptions({strokeWeight: 1.0, fillColor: 'blue'});
                }

            });

        });

    });
}