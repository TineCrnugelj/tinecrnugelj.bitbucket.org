var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
    return "Basic " + btoa(username + ":" + password);
}

// https://www.ehrscape.com/examples.html#_create_a_new_patient
$.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
});

if(document.getElementById('myChart') != null){
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [1,2,3,4,5],
            datasets: [
                {
                    borderColor: 'red',
                    label: 'WEIGHT',
                    data: [1,3,4,5,6],
                },
                {
                    borderColor: 'blue',
                    label: 'BMI',
                    data: [1,3,1,5,6],
                },
                {
                    borderColor: 'green',
                    label: 'HEIGHT',
                    data: [1,3,8,5,6],
                },
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

var pacienti = [
    {
        "firstNames": "Maja",
        "lastNames": "Novak",
        "gender": "MALE",
        "dateOfBirth": "1999-06-24",
        "address": {
            "address": "Tržaška cesta 40, Postojna, Slovenija"
        },
        "partyAdditionalInfo": [
            {"key": "age", "value": 20},
            {"key": "team", "value": "http://www.pamf.org/findadoctor/feature-locations.jpg"},
            {"key": "pic", "value": "http://downloadicons.net/sites/default/files/patient-icon-56370.png"},
            {"key": "allergies", "value": 'Arašidi, Koruza'},
            {"key": "medication", "value": 'Xyzal'},
            {"key": "problems", "value": 'Boleca noga'},
            {"key": "teza", "value": "78"},
            {"key": "bmi", "value": "24.6"},
            {"key": "visina", "value": "178"},
            {"key": "tlak_sis", "value": "80%"},
            {"key": "tlak_dia", "value": "40%"},
            {"key": "o2", "value": "40%"}
        ]
    },
    {
        "firstNames": "Ana",
        "lastNames": "Košir",
        "gender": "MALE",
        "dateOfBirth": "1999-03-03",
        "address": {
            "address": "Slovenska cesta 5, Vrhnika, Slovenija"
        },
        "partyAdditionalInfo": [
            {"key": "age", "value": 20},
            {"key": "team", "value": "http://www.pamf.org/findadoctor/feature-locations.jpg"},
            {"key": "pic", "value": "http://downloadicons.net/sites/default/files/patient-icon-56370.png"},
            {"key": "allergies", "value": 'Pšenica, laktoza'},
            {"key": "medication", "value": 'Xyzal'},
            {"key": "problems", "value": 'Glavoboli'},
            {"key": "teza", "value": "65"},
            {"key": "bmi", "value": "23.9"},
            {"key": "visina", "value": "165"},
            {"key": "tlak_sis", "value": "80%"},
            {"key": "tlak_dia", "value": "40%"},
            {"key": "o2", "value": "98%"},
        ]
    },
    {
        "firstNames": "Majda",
        "lastNames": "Gorenc",
        "gender": "MALE",
        "dateOfBirth": "1999-05-06",
        "address": {
            "address": "Titova ulica 66, Logatec, Slovenija"
        },
        "partyAdditionalInfo": [
            {"key": "age", "value": 20},
            {"key": "team", "value": "http://www.pamf.org/findadoctor/feature-locations.jpg"},
            {"key": "pic", "value": "http://downloadicons.net/sites/default/files/patient-icon-56370.png"},
            {"key": "allergies", "value": 'Banane, oreščki'},
            {"key": "medication", "value": 'Xyzal'},
            {"key": "problems", "value": 'Bolece pete'},
            {"key": "teza", "value": "82"},
            {"key": "bmi", "value": "25.3"},
            {"key": "visina", "value": "180"},
            {"key": "tlak_sis", "value": "80%"},
            {"key": "tlak_dia", "value": "40%"},
            {"key": "o2", "value": "70%"}
        ]
    },
];

function pacientSelected(i){
    var p = pacienti[i];

    $(".patient-name").html(`${p.firstNames} ${p.lastNames}`);
    $(".patient-age").html(p.partyAdditionalInfo[0].value);
    $(".patient-gender").html(p.gender);
    $(".patient-dob").html(p.dateOfBirth);
    $(".patient-address").html(p.address.address);
    $(".patient-team").attr("src", p.partyAdditionalInfo[1].value);
    $(".patient-pic").attr("src", p.partyAdditionalInfo[2].value);

    $(".allergies").html('');
    p.partyAdditionalInfo[3].value.split(',').forEach(function(a){
        $(".allergies").append(`<li>${a}</li>`);
    });

    $(".medications").html('');
    p.partyAdditionalInfo[4].value.split(',').forEach(function(a){
        $(".medications").append(`<li>${a}</li>`);
    });

    $(".problems").html('');
    p.partyAdditionalInfo[5].value.split(',').forEach(function(a){
        $(".problems").append(`<li>${a}</li>`);
    });

    myChart.data.datasets[0].data = []
    p.partyAdditionalInfo[6].value.split(',').forEach(function(t,i){
        if(i==0) $(".patient-teza").html(t);
        myChart.data.datasets[0].data.push(parseFloat(t));
        myChart.update();
    });


    $(".patient-bmi").html(p.partyAdditionalInfo[7].value);

    $(".patient-height").html(p.partyAdditionalInfo[8].value);

    $(".patient-sys").css("width", p.partyAdditionalInfo[9].value);
    $(".patient-dia").css("width", p.partyAdditionalInfo[10].value);

    $(".patient-o2-title").html(p.partyAdditionalInfo[11].value);
    $(".patient-o2").css("width", p.partyAdditionalInfo[11].value);


    $("#result").html(JSON.stringify(pacienti[i]));
}

function showIndex(id){
    $(".index-page").css("display", "none");
    $(id).show();
}

function generirajPodatke() {
    pacienti.forEach(function(pacient, i){
        generirajPacienta(pacient, function(noviPacient){
            console.log(noviPacient);
            pacienti[i] = noviPacient;
            $("#pacienti").append(`<p>${JSON.stringify(noviPacient)}</p>`);
            $("#pacientDropDownMenu").append(`
                <a id="pacient-${i}" onclick="pacientSelected(${i})" class="dropdown-item" href="#!">${noviPacient.firstNames} ${noviPacient.lastNames}</a>
                <br>
            `);

            if(i == pacienti.length-1) {
                pacientSelected(0);
            }
        });
    });
}

function generirajPacienta(pacientInfo, cb) {

    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function(data) {
            var ehrId = data.ehrId;
            console.log(pacientInfo);
            pacientInfo.partyAdditionalInfo.push({
                "key": "ehrId",
                "value": ehrId
            });

            // build party data
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(pacientInfo),
                success: function(party) {
                    if (party.action == 'CREATE') {
                        cb(pacientInfo);
                    }
                }
            });
        }
    });
}

function searchPacient(){
    var ehrId = $("#pacientEHR").val();

    var searchData = [
        {key: "ehrId", value: ehrId}
    ];
    $.ajax({
        url: baseUrl + "/demographics/party/query",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(searchData),
        success: function (res) {
            for (i in res.parties) {
                var party = res.parties[i];
                $("#result").html(`<p>${JSON.stringify(party)}</p>`);

            }
        }
    });
}

